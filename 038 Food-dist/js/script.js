'use strict'

document.addEventListener('DOMContentLoaded', () => {
    const tabsItem = document.querySelectorAll('.tabheader__item'),
        tabsContent = document.querySelectorAll('.tabcontent'),
        tabsParent = document.querySelector('.tabheader');

    function hideTabsContent(){
        tabsContent.forEach(item => {
            item.classList.remove("show", "fade");
            item.classList.add("hide");
        });

        tabsItem.forEach(item => {
            item.classList.remove("tabheader__item_active");
        });
    };

    function showTabContent(id=0){
        tabsItem[id].classList.add("tabheader__item_active");
        tabsContent[id].classList.remove("hide");
        tabsContent[id].classList.add("show", "fade");
    };

    tabsParent.addEventListener('click', function(event) {
        if (event.target && event.target.classList.contains("tabheader__item")){
            hideTabsContent();
            for (var id = 0; id < tabsItem.length; id++){
                if (tabsItem[id] == event.target){
                    showTabContent(id);
                    break;
                }
            }
        }
    });

    hideTabsContent();
    showTabContent();

    //Таймер

    const deadline = '2022-05-20';

    function getTimeRemaining(endtime){
        const t = Date.parse(endtime) - Date.parse(new Date()),
            days = Math.floor(t / 86400000),
            hours = Math.floor((t / 3600000) % 24),
            minutes = Math.floor((t / 60000) % 60),
            seconds = Math.floor((t / 1000) % 60);
        
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };

    };

    function getZero(num){
        if (num >= 0 && num < 10){
            return `0${num}`;
        } else {
            return num;
        }
    }


    function setClock(selector, endtime){
        const timer = document.querySelector(selector),
            days = timer.querySelector('#days'),
            hours = timer.querySelector('#hours'),
            minutes = timer.querySelector('#minutes'),
            seconds = timer.querySelector('#seconds'),
            timeInterval = setInterval(updateClock, 1000);

        updateClock();

        function updateClock(){
            const t = getTimeRemaining(endtime);

            days.innerHTML = getZero(t.days);
            hours.innerHTML = getZero(t.hours);
            minutes.innerHTML = getZero(t.minutes);
            seconds.innerHTML = getZero(t.seconds);

            if (t.total <= 0){
                clearInterval(timeInterval);
            }
        }
    };

    setClock('.timer', deadline);

    //Модальное окно

    const contactBtns = document.querySelectorAll("[data-modal]");
    contactBtns.forEach(item => {
        item.addEventListener('click', showModalWindow)
    });

    const closingBtn = document.querySelector("[data-close]");
    closingBtn.addEventListener('click', hideModalWindow);

    const modalWindow = document.querySelector('.modal');
    modalWindow.addEventListener('click', function(event){
        if (event.target === modalWindow){
            hideModalWindow();
        };
    });

    function showModalWindow(){
        modalWindow.classList.add('show');
        modalWindow.classList.remove('hide');
        document.body.style.overflow = 'hidden';
        clearInterval(modalTimer);
    };

    function hideModalWindow(){
        modalWindow.classList.add('hide');
        modalWindow.classList.remove('show');
        document.body.style.overflow = '';
    }

    //Модификация модального окна

    const modalTimer = setTimeout(showModalWindow, 5000);

    function showModalByScroll(){
        if (window.pageYOffset + document.documentElement.clientHeight >= document.documentElement.scrollHeight){
            showModalWindow();
            window.removeEventListener('scroll', showModalByScroll);
        }
    }

    window.addEventListener('scroll', showModalByScroll);

    //Карточки

    class MenuCard{
        constructor(imgSrc, altText, title, descr, price, parent, ...classes){
            this.imgSrc = imgSrc;
            this.altText = altText;
            this.title = title;
            this.descr = descr;
            this.price = price;
            this.parent = document.querySelector(parent);
            this.classes = classes;
        }

        makeLayout(){
            const cardDiv = document.createElement('div');

            if (this.classes.length > 0){
                this.classes.forEach(className => cardDiv.classList.add(className));
            } else {
                cardDiv.classList.add('menu__item');
            }
            cardDiv.innerHTML = `
                <img src=${this.imgSrc} alt=${this.altText}>
                <h3 class="menu__item-subtitle">Меню "${this.title}"</h3>
                <div class="menu__item-descr">${this.descr}</div>
                <div class="menu__item-divider"></div>
                <div class="menu__item-price">
                    <div class="menu__item-cost">Цена:</div>
                    <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
                </div>
                `;

            this.parent.append(cardDiv);

        }
    }

    let cards = [new MenuCard(  'img/tabs/vegy.jpg',
                                'vegy',
                                'Фитнес',
                                'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
                                229,
                                '.menu .container',
                                'menu__item'
                                ),
                new MenuCard(  'img/tabs/elite.jpg',
                                'elite',
                                'Премиум',
                                'В меню “Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!',
                                550,
                                '.menu .container',
                                'menu__item'
                                ),
                new MenuCard(  'img/tabs/post.jpg',
                                'post',
                                'Постное',
                                'Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков.',
                                430,
                                '.menu .container',
                                'menu__item'
                                )
                ];

    for (let item of cards){
        item.makeLayout();
    }

    //Слайдеры

    const sliderItems = document.querySelectorAll('.offer__slide'),
          prevSlider = document.querySelector('.offer__slider-prev'),
          nextSlider = document.querySelector('.offer__slider-next');
    
    let currentCount = document.querySelector('#current'),
        totalCount = document.querySelector('#total'),
        sliderId = 0;
    
    function hideSliders(){
        sliderItems.forEach(item => {
            item.classList.remove("show", "fade");
            item.classList.add("hide");
        });
        totalCount.innerText = getZero(sliderItems.length);
    }

    function showSlider(){
        sliderId = (sliderId + sliderItems.length) % sliderItems.length;
        sliderItems[sliderId].classList.remove("hide");
        sliderItems[sliderId].classList.add("show", "fade");
        currentCount.innerText = getZero(sliderId+1);
    }

    prevSlider.addEventListener('click', function(){
        sliderId--;
        hideSliders();
        showSlider();
    });

    nextSlider.addEventListener('click', function(){
        sliderId++;
        hideSliders();
        showSlider();
    });

    hideSliders();
    showSlider();

});